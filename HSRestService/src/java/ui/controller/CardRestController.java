/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.controller;

import db.MapperException;
import domain.Card;
import domain.HearthstoneService;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author niels
 */
@Path("card")
public class CardRestController {
    HearthstoneService service = new HearthstoneService("jpa");
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void addCard(Card card) throws MapperException {
        service.addCard(card);
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Collection<Card> getCards() {
        return service.getCards();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}")
    public Card getCard(@PathParam("id") long id) throws MapperException {
        return service.getCard(id);
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void updateCard(Card card) {
        try {
            service.updateCard(card);
        } catch (MapperException ex) {
            Logger.getLogger(CardRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @DELETE
    @Path("/{id}")
    public void removeCard(@PathParam("id") long id){
        try {
            service.removeCard(id);
        } catch (MapperException ex) {
            Logger.getLogger(CardRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
