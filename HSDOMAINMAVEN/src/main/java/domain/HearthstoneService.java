/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import db.MapperException;
import db.MapperLocator;
import db.MapperLocatorFactory;
import java.util.Collection;

/**
 *
 * @author niels
 */
public class HearthstoneService {
    
    public HearthstoneService(String db) {
        MapperLocatorFactory.createMapperLocator(db);
    }
    
    public long addCard(Card card) throws MapperException {
        return MapperLocator.cardMapper().store(card);
    }
    
    public Card getCard(long id) throws MapperException {
        return MapperLocator.cardMapper().find(id);
    }
    
    public void removeCard(long id) throws MapperException {
        MapperLocator.cardMapper().delete(id);
    }
    
    public void updateCard(Card card) throws MapperException {
        MapperLocator.cardMapper().update(card);
    }
    
    public Collection<Card> getCards() {
        return MapperLocator.cardMapper().findAll();
    }
    
    public long addDeck(Deck deck) throws MapperException {
        return MapperLocator.deckMapper().store(deck);
    }
    
    public void removeDeck(long id) throws MapperException {
        MapperLocator.deckMapper().delete(id);
    }
    
    public void updateDeck(Deck deck) throws MapperException {
        MapperLocator.deckMapper().update(deck);
    }
    
    public Deck getDeck(long id) throws MapperException {
        return MapperLocator.deckMapper().find(id);
    }
    
    public Collection<Deck> getDecks() {
        return MapperLocator.deckMapper().findAll();
    }

    void removeCard(Card card) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
