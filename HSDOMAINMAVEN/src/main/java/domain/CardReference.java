package domain;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Entity;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@Entity

public class CardReference extends DomainObjectBase {

    @ManyToOne
    @JoinColumn(name = "deck_id")
    private Deck deck;
    private Long cardId;
    @Min(value=1, message="{error.cardReference.count.min}")
    @Max(value=2, message="{error.cardReference.count.max}")
    private int count = 1;

    public CardReference() {
    }

    public CardReference(long cardId, Deck deck) {
        this.cardId = cardId;
        this.deck = deck;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    
    public Long getCardId() {
        return cardId;
    }

    
    public void setCardId(long id) {
        this.cardId = id;
    }

    @Override
    public boolean equals(Object d) {
        if (d instanceof CardReference) {
            return equals((CardReference) d);
        }
        return d.equals(this);
    }

    public boolean equals(CardReference d) {
        return (d.getCardId().equals(getCardId())
                && (getClass().isInstance(d)
                || d.getClass().isInstance(this)));
    }

    @Override
    public int hashCode() {
        return getCardId().hashCode();
    }
}
