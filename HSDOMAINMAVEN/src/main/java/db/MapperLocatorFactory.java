/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import db.memory.MemoryMapperLocator;
import db.relational.RelationalMapperLocator;

/**
 *
 * @author niels
 */
public class MapperLocatorFactory {
    
    public static void createMapperLocator(String type) {
        if(type.equals("memory")) {
            MapperLocator.loadLocator(new MemoryMapperLocator());
        } else {
            MapperLocator.loadLocator(new RelationalMapperLocator());
        }
    }
    
}
