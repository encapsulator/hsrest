/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.memory;

import db.Mapper;
import db.MapperException;
import domain.Deck;
import domain.DomainException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author niels
 */
public class MemoryDeckMapper implements Mapper<Deck> {

    private Map<Long, Deck> decks;
    private int id;
    public MemoryDeckMapper() {
        decks = new HashMap<>();
        Deck deck = new Deck();
        id = 1;
        deck.setId(id);
        deck.setName("PriestDeck");
        deck.setPlayerClass("Priest");
        try {
            deck.addCard(1);
        } catch (DomainException ex) {
            Logger.getLogger(MemoryDeckMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        decks.put((long)id, deck);
    }
    
    @Override
    public long store(Deck domainObject) throws MapperException {
        domainObject.setId(++id);
        decks.put(domainObject.getId(), domainObject);
        return domainObject.getId();
    }

    @Override
    public void update(Deck domainObject) throws MapperException {
        Deck deck = decks.get(domainObject.getId());
        if(deck == null) {
            throw new MapperException("Deck with id " + domainObject.getId() + " not found");
        }
        decks.remove(domainObject.getId());
        decks.put(domainObject.getId(), domainObject);
    }

    @Override
    public void delete(long id) throws MapperException {
        Deck deck = decks.get(id);
        if(deck == null) {
            throw new MapperException("Deck with id " + id + " not found");
        }
        decks.remove(id);
    }

    @Override
    public Deck find(long id) throws MapperException {
        Deck deck = decks.get(id);
        if(deck == null) {
            throw new MapperException("Deck with id " + id + " not found");
        }
        return deck;
    }

    @Override
    public Collection<Deck> findAll() {
        return decks.values();
    }

    @Override
    public void close() throws MapperException {
        decks.clear();
    }
    
    
    @Override
    public void clear() throws MapperException {
        decks.clear();
    }
}
