/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import domain.Card;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author niels
 */
public class RelationalCardMapper extends RelationalMapper<Card>{

    public RelationalCardMapper() {
        super(Card.class);
    }
    
     @Override
    public List<Card> findAll() {
      
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<Card> lQuery = em.createQuery("from Card c ORDER BY c.cost DESC", Card.class);
            return lQuery.getResultList();
        } catch (Exception ex) {
            return new LinkedList<>();
        } finally {
            em.close();

        }
    }
    
}
