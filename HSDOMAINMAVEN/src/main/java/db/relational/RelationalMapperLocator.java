/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import db.MapperLocator;

/**
 *
 * @author niels
 */
public class RelationalMapperLocator extends MapperLocator {
    
    public RelationalMapperLocator() {
        cardMapper = new RelationalCardMapper();
        deckMapper = new RelationalDeckMapper();
    }
    
}
