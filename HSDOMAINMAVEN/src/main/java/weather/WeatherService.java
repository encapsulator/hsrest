/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author niels
 */
public class WeatherService {
    
    public static Current getWeather(String city) {
        Client client = ClientBuilder.newClient();
        WebTarget service = client.target("http://api.openweathermap.org/data/2.5/weather")
                .queryParam("q", city)
                .queryParam("mode", "xml");

        return service.request(MediaType.APPLICATION_XML).get(Current.class);

    }
}
