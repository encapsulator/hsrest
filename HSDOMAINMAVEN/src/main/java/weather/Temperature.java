/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author niels
 */
public class Temperature {
    private double value;
    
    public Temperature() {}

    public Temperature(double value) {
        this.value = value;
    }
    
    public double getValue() {
        return value;
    }
    
    @XmlAttribute
    public void setValue(double value) {
        this.value = (double) Math.round((value-274.15)*100)/100;
    }

}
