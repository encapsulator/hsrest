/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import db.MapperException;
import domain.Card;
import domain.DomainException;
import static junit.framework.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author niels
 */
public class RelationalCardMapperTest {

    private final RelationalCardMapper mapper;
    private final Card card;
    private long persistedId;

    public RelationalCardMapperTest() throws DomainException {
        this.mapper = new RelationalCardMapper();
        this.card = new Card("Ragnaros", "Minion", "Can't Attack. At the end of turn deal 8 damage to a random enemy character", "All classes", 8, 8, 8);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() throws MapperException {

        mapper.delete(persistedId);

        mapper.close();
    }

    @Test
    public void testStoreCard_CardIsPersisted() throws MapperException {
        persistedId = mapper.store(card);
        Card persisted = mapper.find(persistedId);
        assertEquals(card.getName(), persisted.getName());
    }

}
