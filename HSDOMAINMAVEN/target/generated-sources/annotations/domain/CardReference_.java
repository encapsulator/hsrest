package domain;

import domain.Deck;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2015-05-11T14:41:46")
@StaticMetamodel(CardReference.class)
public class CardReference_ extends DomainObjectBase_ {

    public static volatile SingularAttribute<CardReference, Deck> deck;
    public static volatile SingularAttribute<CardReference, Long> cardId;
    public static volatile SingularAttribute<CardReference, Integer> count;

}