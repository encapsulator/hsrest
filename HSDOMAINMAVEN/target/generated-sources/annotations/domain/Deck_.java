package domain;

import domain.CardReference;
import domain.DeckStatus;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2015-05-11T14:41:46")
@StaticMetamodel(Deck.class)
public class Deck_ extends DomainObjectBase_ {

    public static volatile SingularAttribute<Deck, String> playerClass;
    public static volatile ListAttribute<Deck, CardReference> cards;
    public static volatile SingularAttribute<Deck, String> name;
    public static volatile SingularAttribute<Deck, DeckStatus> status;

}