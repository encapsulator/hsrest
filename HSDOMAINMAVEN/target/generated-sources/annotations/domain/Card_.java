package domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2015-05-11T14:41:46")
@StaticMetamodel(Card.class)
public class Card_ extends DomainObjectBase_ {

    public static volatile SingularAttribute<Card, String> playerClass;
    public static volatile SingularAttribute<Card, Integer> cost;
    public static volatile SingularAttribute<Card, Integer> attack;
    public static volatile SingularAttribute<Card, String> name;
    public static volatile SingularAttribute<Card, Integer> health;
    public static volatile SingularAttribute<Card, String> text;
    public static volatile SingularAttribute<Card, String> type;

}